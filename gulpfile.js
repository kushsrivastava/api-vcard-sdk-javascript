'use strict';

var gulp = require('gulp');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');

var _config = {
    name: 'vcard-sdk',
    latest: '1.3',
    src: 'src',
    dest: 'dist'
}

gulp.task('build', function () {
    
    gulp.src(_config.src + '/*.js')
        .pipe(uglify())
        .pipe(rename(function (path) {
            path.basename = _config.name + '.' + path.basename + '.min';
        }))
        .pipe(gulp.dest(_config.dest));
    
    gulp.src(_config.src + '/' + _config.latest + '.js')
        .pipe(uglify())
        .pipe(rename(function (path) {
            path.basename = _config.name + '.latest.min';
        }))
        .pipe(gulp.dest(_config.dest));
});